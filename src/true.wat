(module

(import "wasi_snapshot_preview1" "proc_exit"
  (func $proc_exit (param u32))
)

(memory (export "memory") 0)

(func (export "_start")
	(call $proc_exit (i32.const 0))
)

)